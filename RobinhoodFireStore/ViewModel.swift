//
//  ViewModel.swift
//  RobinhoodFireStore
//
//  Created by Purananunaka, Chanon on 11/3/2565 BE.
//

import Foundation
import Combine

class ViewModel: ObservableObject {
//  @Published var
  
  private var cancellableSet: Set<AnyCancellable> = []
  var dataManager: ServiceProtocol
  init(dataManager: ServiceProtocol = Service.shared) {
    self.dataManager = dataManager
  }
  
  func callFireStore(collectionName: String, orderId: String, fields: RequestBodyModel.Fields) -> String {
    return dataManager.callFireStore(collectionName: collectionName, orderId: orderId, fields: fields)
//      .sink { dataResponse in
//        print("CPK: \(dataResponse)")
//    }.store(in: &cancellableSet)
  }
  
  func callTopUpVA(fields: RequestBodyForVAModel.Fields) -> String {
    return dataManager.topUpVirtualAccount(fields: fields)
  }
  
  func callNotification(fields: RequestBodyForNotificationModel.Fields) -> String {
    return dataManager.sendNotification(fields: fields)
  }
}
