//
//  ContentView.swift
//  RobinhoodFireStore
//
//  Created by Purananunaka, Chanon on 11/3/2565 BE.
//

import SwiftUI
import Combine

struct ContentView: View {
  var body: some View {
    TabView {
      FireStore()
      TopUpVirtualAccountView()
      NotificationView()
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
