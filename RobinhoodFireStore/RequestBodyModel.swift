//
//  RequestBodyModel.swift
//  RobinhoodFireStore
//
//  Created by Purananunaka, Chanon on 11/3/2565 BE.
//

import Foundation

public struct RequestBodyForVAModel {
  var fields: Fields
  
  struct Fields {
    var userId: String
    var currency: Currencies
    var amount: String
    var channel: Channel
    var service: Service
  }
  
  enum Currencies: String, CodingKey {
    case THB = "THB"
  }
  
  enum Channel: String, CodingKey {
    case SPEND = "SPEND"
  }
  
  enum Service: String, CodingKey {
    case FOOD = "FOOD"
    case MART = "MART"
  }
}

public struct RequestBodyForNotificationModel {
  var fields: Fields
  
  struct Fields {
    var userId: String
    var category: Category
    var subjectTh: String
    var subjectEn: String
    var bodyTh: String
    var bodyEn: String
    var deeplinkType: DeeplinkType
    var deeplinkLabelTh: String
    var deeplinkLabelEn: String
    var deeplinkTh: String
    var deeplinkEn: String
    var clickAction: String
    var notificationType: NotificationType
  }
  
  enum Category: Int, CodingKey {
    case PROMOTION = 1, ANNOUNCEMENT, E_RECEIPT, ONBOARD, ORDER_TRACKING, OTHERS
    var description: String {
      switch self {
      case .PROMOTION: return "PROMOTION"
      case .ANNOUNCEMENT: return "ANNOUNCEMENT"
      case .E_RECEIPT: return "E_RECEIPT"
      case .ONBOARD: return "ONBOARD"
      case .ORDER_TRACKING: return "ORDER_TRACKING"
      case .OTHERS: return "OTHERS"
      }
    }
  }
  
  enum DeeplinkType: Int, CodingKey {
    case DEEPLINK = 1, WEBVIEW, EXTERNAL, INBOX_WEBVIEW, INBOX_DEEPLINK
    var description: String {
      switch self {
      case .DEEPLINK: return "DEEPLINK"
      case .WEBVIEW: return "WEBVIEW"
      case .EXTERNAL: return "EXTERNAL"
      case .INBOX_WEBVIEW: return "INBOX_WEBVIEW"
      case .INBOX_DEEPLINK: return "INBOX_DEEPLINK"
      }
    }
  }
  
  enum NotificationType: Int, CodingKey {
    case NewsPromotion = 1, SpecialForYou
    var description: String {
//      return String(describing: self)
      switch self {
      case .NewsPromotion: return "News/Promotion"
      case .SpecialForYou: return "Special For You"
      }
    }
  }
}

public struct RequestBodyModel {
  var fields: Fields
  
  struct Fields {
    var status: OrderStatus
    var state: OrderState
    var statusDescription: StatusDescription
    var updateDateTime: UpdateDatetime
    var enableCancel: EnableCancel
    var channel: PaymentChannel
    var type: PaymentType
    
    public init(status: OrderStatus,
                state: OrderState,
                statusDescription: StatusDescription,
                updateDateTime: UpdateDatetime,
                enableCancel: EnableCancel,
                channel: PaymentChannel,
                type: PaymentType){
      self.status = status
      self.state = state
      self.statusDescription = statusDescription
      self.updateDateTime = updateDateTime
      self.enableCancel = enableCancel
      self.channel = channel
      self.type = type
    }
    
    func toJSON() -> [String: Any] {
      var json: [String: Any] = [:]
      json["status"] = status
      json["state"] = state
      json["statusDescription"] = statusDescription
      json["updateDateTime"] = updateDateTime
      json["enableCancel"] = enableCancel
      json["channel"] = channel
      json["type"] = type
      return json
    }
  }
  
  struct StatusDescription {
    var mapValue: MapValue
    public init(mapValue: MapValue) {
      self.mapValue = mapValue
    }
  }
  
  struct MapValue {
    var fields: WordingFields
    public init(fields: WordingFields) {
      self.fields = fields
    }
  }
  
  struct WordingFields {
    var th: TH
    var en: EN
    public init(th: TH, en: EN){
      self.th = th
      self.en = en
    }
  }
  
  struct TH {
    var stringValue: String
    public init(stringValue: String) {
      self.stringValue = stringValue
    }
  }
  
  struct EN {
    var stringValue: String
    public init(stringValue: String) {
      self.stringValue = stringValue
    }
  }
  
  struct UpdateDatetime {
    var stringValue: String
    public init(stringValue: String) {
      self.stringValue = stringValue
    }
  }
  
  struct EnableCancel {
    var booleanValue: Bool
    public init(booleanValue: Bool) {
      self.booleanValue = booleanValue
    }
  }
}

enum PaymentType: String, CodingKey {
  case credit = "CREDIT_CARD"
  case debit = "DEBIT_CARD"
  case prepaid = "PREPAID_CARD"
  case other = "OTHER"
}

enum PaymentChannel: String, CodingKey {
  case wallet = "VA"
  case card = "CARD"
  case paywise = "PAYWISE"
  case paywiseWithWallet = "PAYWISE_VA"
  case cardWithWallet = "CARD_VA"
  case kbank = "KBANK"
  case trueMoney = "TRUEMONEY"
  case shopeePay = "SHPPAY"
}

enum OrderStatus: String, CodingKey {
    case PROCESS = "PROCESS"
    case FAIL = "FAIL"
    case FINDING = "FINDING"
    case CANCEL = "CANCEL"
    case NOTFOUND = "NOTFOUND"
    case TIMEOUT = "TIMEOUT"
    case FOUND = "FOUND"
    case REJECT = "REJECT"
    case ACCEPT = "ACCEPT"
    case DEPARTURE = "DEPARTURE"
    case WAITING = "WAITING"
    case PICKEDUP = "PICKEDUP"
    case NEARBY = "NEARBY"
    case COMPLETE = "COMPLETE"
    case ARRIVED = "ARRIVED"
}

enum OrderState: String, CodingKey {
    case PAYMENT = "PAYMENT"
    case DRIVER = "DRIVER"
    case CUSTOMER = "CUSTOMER"
    case MERCHANT = "MERCHANT"
    case ADMIN = "ADMIN"
}

enum StateStatusMessage: String {
    case paymentProcess = "กำลังดำเนินการ"
    case paymentFail = "ชำระเงินไม่สำเร็จ"
    case driverFinding = "กำลังหาคนขับ"
    case customerCancel = "ลูกค้ายกเลิก"
    case driverNotFound = "แย่จังไม่พบคนขับ"
    case driverTimeout = "ขออภัย ไม่พบคนขับ กรุณาทำรายการสั่งซื้อใหม่"
    case driverFound = "พบคนขับแล้ว"
    case merchantTimeout = "ขออภัย ร้านค้าไม่สามารถรับคำสั่งซื้อของคุณได้ในขณะนี้ เนื่องจากทางร้านมีคำสั่งซื้อเข้ามาจำนวนมาก"
    case merchantReject = "ขออภัย ร้านค้าไม่สามารถรับคำสั่งซื้อของคุณได้"
    case merchantAccept = "ร้านค้าได้รับคำสั่งซื้อแล้ว กำลังเตรียมอาหารของคุณ"
    case driverDeparture = "คนขับกำลังมุ่งหน้าไปที่ร้าน"
    case driverWaiting = "คนขับถึงร้านแล้ว อดใจรอสักครู่"
    case driverPickedUp = "คนขับกำลังนำอาหารไปส่งคุณ"
    case driverNearBy = "คนขับใกล้จะถึงแล้ว คุณพร้อมหรือยัง?"
    case driverArrived = "คนขับมาถึงแล้ว"
    case driverComplete = "สำเร็จ"
    case adminCancel = "ขออภัย ออเดอร์ของคุณไม่สามารถดำเนินการต่อได้ในขณะนี้"
}

enum StateStatusCase: String {
  case paymentProcess = "PAYMENT,PROCESS"
  case paymentFail = "PAYMENT,FAIL"
  case driverFinding = "DRIVER,FINDING"
  case customerCancel = "CUSTOMER,CANCEL"
  case driverNotFound = "DRIVER,NOTFOUND"
  case driverTimeout = "DRIVER,TIMEOUT"
  case driverFound = "DRIVER,FOUND"
  case merchantTimeout = "MERCHANT,TIMEOUT"
  case merchantReject = "MERCHANT,REJECT"
  case merchantAccept = "MERCHANT,ACCEPT"
  case driverDeparture = "DRIVER,DEPARTURE"
  case driverWaiting = "DRIVER,WAITING"
  case driverPickedUp = "DRIVER,PICKEDUP"
  case driverNearBy = "DRIVER,NEARBY"
  case driverArrived = "DRIVER,ARRIVED"
  case driverComplete = "DRIVER,COMPLETE"
  case adminCancel = "ADMIN,CANCEL"
}

public enum OrderTrackingStatus: Int {
  case paymentProcess = 1
  case paymentFail
  case driverFinding
  case customerCancel
  case driverNotFound
  case driverTimeout
  case driverFound
  case merchantTimeout
  case merchantReject
  case merchantAccept
  case driverDeparture
  case driverWaiting
  case driverPickedUp
  case driverNearBy
  case driverArrived
  case driverComplete
  case adminCancel
}
