//
//  NotificationView.swift
//  RobinhoodFireStore
//
//  Created by Purananunaka, Chanon on 27/6/2565 BE.
//

import SwiftUI
import Combine

struct NotificationView: View {
  @State private var showingAlert = false
  @State var result: String = ""
  @ObservedObject var viewModel = ViewModel()
  
  //Notification
  @State var userId: String = "0a66ce5f-b7d6-481d-b67a-00b460e84c1a-InQKrdqI"
  @State var category: RequestBodyForNotificationModel.Category = .E_RECEIPT
  var categoryValue: [RequestBodyForNotificationModel.Category] = [.PROMOTION,.ANNOUNCEMENT,.E_RECEIPT,.ONBOARD,.ORDER_TRACKING,.OTHERS]
  @State var subjectTh: String = "เปิดหน้า Food Landing"
  @State var subjectEn: String = "Navigate to Food Landing"
  @State var bodyTh: String = "กดเพื่อทำการเปิดหน้า Food Landing"
  @State var bodyEn: String = "Press for navigate to Food Landing"
  @State var deeplinkType: RequestBodyForNotificationModel.DeeplinkType = .INBOX_DEEPLINK
  var deeplinkTypeValue: [RequestBodyForNotificationModel.DeeplinkType] = [.DEEPLINK,.WEBVIEW,.EXTERNAL,.INBOX_WEBVIEW,.INBOX_DEEPLINK]
  @State var deeplinkLabelTh: String = "กดเพื่อทำการเปิดหน้า Food Landing"
  @State var deeplinkLabelEn: String = "Press for navigate to Food Landing"
  @State var deeplinkTh: String = "robinhoodth://foodlanding"
  @State var deeplinkEn: String = "robinhoodth://foodlanding"
  @State var clickAction: String = "robinhood.receive.FCM"
  @State var notificationType: RequestBodyForNotificationModel.NotificationType = .SpecialForYou
  var notificationTypeValue: [RequestBodyForNotificationModel.NotificationType] = [.NewsPromotion,.SpecialForYou]
  
  @State private var fitInScreen = false
  
  var body: some View {
    //Begin Notification
    ScrollView(.vertical, showsIndicators: true, content: {
      VStack(alignment: .leading, spacing: 4) {
        Group {
          VStack(alignment: .leading, spacing: 0) {
            Text("For DEV2 only").frame(alignment: .center)
            Text("UserId: ")
            TextEditor(text: $userId)
              .multilineTextAlignment(.leading)
              .lineLimit(5)
              .font(.body)
              .frame(height: 80, alignment: .topLeading)
              .border(.black, width: 1)
              .cornerRadius(2)
              .foregroundColor(.black)
              .onChange(of: userId, perform: { value in
                
              })
          }
          
          Text("Category: ")
          Menu {
            ForEach(categoryValue, id: \.self){ value in
              Button("\(value.description)") {
                self.category = value
              }
            }
          } label: {
              VStack(spacing: 5){
                  HStack{
                    Text("\(category.description)")
                      Spacer()
                      Image(systemName: "chevron.down")
                      .foregroundColor(Color.primary)
                      .font(Font.system(size: 20, weight: .bold))
                  }
                  .padding(.horizontal)
                  Rectangle()
                  .fill(Color.primary)
                  .frame(height: 2)
              }
          }
          
          VStack(alignment: .leading, spacing: 0) {
            Text("subjectTh: ")
            TextEditor(text: $subjectTh)
              .multilineTextAlignment(.leading)
              .lineLimit(5)
              .font(.body)
              .frame(height: 80, alignment: .topLeading)
              .border(.black, width: 1)
              .cornerRadius(2)
              .foregroundColor(.black)
              .onChange(of: subjectTh, perform: { value in
                
              })
          }
          
          VStack(alignment: .leading, spacing: 0) {
            Text("subjectEn: ")
            TextEditor(text: $subjectEn)
              .multilineTextAlignment(.leading)
              .lineLimit(5)
              .font(.body)
              .frame(height: 80, alignment: .topLeading)
              .border(.black, width: 1)
              .cornerRadius(2)
              .foregroundColor(.black)
              .onChange(of: subjectEn, perform: { value in
                
              })
          }
        }
        
        Group {
          VStack(alignment: .leading, spacing: 0) {
            Text("bodyTh: ")
            TextEditor(text: $bodyTh)
              .multilineTextAlignment(.leading)
              .lineLimit(5)
              .font(.body)
              .frame(height: 80, alignment: .topLeading)
              .border(.black, width: 1)
              .cornerRadius(2)
              .foregroundColor(.black)
              .onChange(of: bodyTh, perform: { value in
                
              })
          }
          
          VStack(alignment: .leading, spacing: 0) {
            Text("bodyEn: ")
            TextEditor(text: $bodyEn)
              .multilineTextAlignment(.leading)
              .lineLimit(5)
              .font(.body)
              .frame(height: 80, alignment: .topLeading)
              .border(.black, width: 1)
              .cornerRadius(2)
              .foregroundColor(.black)
              .onChange(of: bodyEn, perform: { value in
                
              })
          }
        }
        
        Text("DeeplinkType: ")
        Menu {
          ForEach(deeplinkTypeValue, id: \.self){ value in
            Button("\(value.description)") {
              self.deeplinkType = value
            }
          }
        } label: {
            VStack(spacing: 5){
                HStack{
                  Text("\(deeplinkType.description)")
                    Spacer()
                    Image(systemName: "chevron.down")
                    .foregroundColor(Color.primary)
                    .font(Font.system(size: 20, weight: .bold))
                }
                .padding(.horizontal)
                Rectangle()
                .fill(Color.primary)
                .frame(height: 2)
            }
        }
        
        Group {
          VStack(alignment: .leading, spacing: 0) {
            Text("deeplinkLabelTh: ")
            TextEditor(text: $deeplinkLabelTh)
              .multilineTextAlignment(.leading)
              .lineLimit(5)
              .font(.body)
              .frame(height: 80, alignment: .topLeading)
              .border(.black, width: 1)
              .cornerRadius(2)
              .foregroundColor(.black)
              .onChange(of: deeplinkLabelTh, perform: { value in
                
              })
          }
          
          VStack(alignment: .leading, spacing: 0) {
            Text("deeplinkLabelEn: ")
            TextEditor(text: $deeplinkLabelEn)
              .multilineTextAlignment(.leading)
              .lineLimit(5)
              .font(.body)
              .frame(height: 80, alignment: .topLeading)
              .border(.black, width: 1)
              .cornerRadius(2)
              .foregroundColor(.black)
              .onChange(of: deeplinkLabelEn, perform: { value in
                
              })
          }
        }
        
        Group {
          VStack(alignment: .leading, spacing: 0) {
            Text("deeplinkTh: ")
            TextEditor(text: $deeplinkTh)
              .multilineTextAlignment(.leading)
              .lineLimit(5)
              .font(.body)
              .frame(height: 80, alignment: .topLeading)
              .border(.black, width: 1)
              .cornerRadius(2)
              .foregroundColor(.black)
              .onChange(of: deeplinkLabelTh, perform: { value in
                
              })
          }
          
          VStack(alignment: .leading, spacing: 0) {
            Text("deeplinkEn: ")
            TextEditor(text: $deeplinkEn)
              .multilineTextAlignment(.leading)
              .lineLimit(5)
              .font(.body)
              .frame(height: 80, alignment: .topLeading)
              .border(.black, width: 1)
              .cornerRadius(2)
              .foregroundColor(.black)
              .onChange(of: deeplinkEn, perform: { value in
                
              })
          }
          
          VStack(alignment: .leading, spacing: 0) {
            Text("clickAction: ")
            TextEditor(text: $clickAction)
              .multilineTextAlignment(.leading)
              .lineLimit(5)
              .font(.body)
              .frame(height: 80, alignment: .topLeading)
              .border(.black, width: 1)
              .cornerRadius(2)
              .foregroundColor(.black)
              .onChange(of: deeplinkEn, perform: { value in
                
              })
          }
        }
        
        Text("Notification Type: ")
        Menu {
          ForEach(notificationTypeValue, id: \.self){ value in
            Button("\(value.description)") {
              self.notificationType = value
            }
          }
        } label: {
            VStack(spacing: 5){
                HStack{
                  Text("\(notificationType.description)")
                    Spacer()
                    Image(systemName: "chevron.down")
                    .foregroundColor(Color.primary)
                    .font(Font.system(size: 20, weight: .bold))
                }
                .padding(.horizontal)
                Rectangle()
                .fill(Color.primary)
                .frame(height: 2)
            }
        }
        
        Button("Send notification  ", action: {
          if userId != "",
             category.rawValue > 0 && category.rawValue < 7,
             subjectTh != "",
             subjectEn != "",
             bodyTh != "",
             bodyEn != "",
             deeplinkType.rawValue > 0 && deeplinkType.rawValue < 6,
             deeplinkLabelTh != "",
             deeplinkLabelEn != "",
             deeplinkTh != "",
             deeplinkEn != "",
             clickAction != "",
             notificationType.rawValue > 0 && notificationType.rawValue < 3 {
            let fields: RequestBodyForNotificationModel.Fields = RequestBodyForNotificationModel.Fields(userId: userId,
                                                                                                        category: category,
                                                                                                        subjectTh: subjectTh,
                                                                                                        subjectEn: subjectEn,
                                                                                                        bodyTh: bodyTh,
                                                                                                        bodyEn: bodyEn,
                                                                                                        deeplinkType: deeplinkType,
                                                                                                        deeplinkLabelTh: deeplinkLabelTh,
                                                                                                        deeplinkLabelEn: deeplinkLabelEn,
                                                                                                        deeplinkTh: deeplinkTh,
                                                                                                        deeplinkEn: deeplinkEn,
                                                                                                        clickAction: clickAction,
                                                                                                        notificationType: notificationType)
            result = viewModel.callNotification(fields: fields)
            showingAlert = true
          }
        }).buttonStyle(.bordered).alert(result, isPresented: $showingAlert) {
          Button("OK", role: .cancel) {}
        }.font(.system(size: 14))
      }.padding().frame(width: UIScreen.main.bounds.width,
                        height: .infinity,
                        alignment: .topLeading)
    })
    .tabItem {
      Image(systemName: "bubble.right.fill")
      Text("Send Notification")
    }.onTapGesture {
      UIApplication.shared.endEditing()
    }//End Notification
  }//End body
}
