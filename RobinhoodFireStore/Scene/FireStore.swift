//
//  FireStore.swift
//  RobinhoodFireStore
//
//  Created by Purananunaka, Chanon on 28/6/2565 BE.
//

import SwiftUI
import Combine

struct FireStore: View {
  @State private var showingAlert = false
  @State var result: String = ""
  @ObservedObject var viewModel = ViewModel()
  
  //FireStore
  @State var stateStatusCase: StateStatusCase = .driverFinding
  @State var status: OrderStatus = .FINDING
  @State var state: OrderState = .DRIVER
  @State var orderId: String = "M220302-AA-0174"
  @State var stateStatusMessageTH: String = ""
  @State var stateStatusMessageEN: String = ""
  @State var updateDatetime: String = "25 นาที"
  @State var enableCancel: Bool = false
  @State var collectionName: String = ""
  @State var environment: String = ""
  @State var channel: PaymentChannel = .card
  @State var type: PaymentType = .credit
  
  var placeholder = "Select State & Status"
  var dropDownList: [StateStatusCase] = [.driverFinding, .paymentProcess, .paymentFail, .customerCancel, .driverNotFound, .driverTimeout, .driverFound, .merchantTimeout, .merchantReject, .merchantAccept, .driverDeparture, .driverWaiting, .driverPickedUp, .driverNearBy, .driverArrived, .driverComplete, .adminCancel]
  var enableCancelValue: [Bool] = [true, false]
  var environmentValue: [String] = ["","-dev","-dev2","-qa"]
  var collectionNameValue: [String] = ["order-status-updates","mart-status-updates"]
  var channelList: [PaymentChannel] = [.card, .cardWithWallet, .paywise, .paywiseWithWallet, .wallet, .shopeePay, .trueMoney, .kbank]
  var typeList: [PaymentType] = [.credit, .debit, .prepaid, .other]
  
  var body: some View {
    //Begin FireStore
    ScrollView(.vertical, showsIndicators: true, content: {
      VStack(alignment: .leading, spacing: 4) {
        Text("Robinhood FireStore SwiftUI").foregroundColor(.indigo)
        Menu {
          ForEach(dropDownList, id: \.self) { stateStatusCase in
            Button(stateStatusCase.rawValue, action: {
              self.stateStatusCase = stateStatusCase
              switch stateStatusCase {
              case .paymentProcess:
                stateStatusMessageTH = StateStatusMessage.paymentProcess.rawValue
                stateStatusMessageEN = StateStatusMessage.paymentProcess.rawValue
                state = .PAYMENT
                status = .PROCESS
              case .paymentFail:
                stateStatusMessageTH = StateStatusMessage.paymentFail.rawValue
                stateStatusMessageEN = StateStatusMessage.paymentFail.rawValue
                state = .PAYMENT
                status = .FAIL
              case .driverFinding:
                stateStatusMessageTH = StateStatusMessage.driverFinding.rawValue
                stateStatusMessageEN = StateStatusMessage.driverFinding.rawValue
                state = .DRIVER
                status = .FINDING
              case .customerCancel:
                stateStatusMessageTH = StateStatusMessage.customerCancel.rawValue
                stateStatusMessageEN = StateStatusMessage.customerCancel.rawValue
                state = .CUSTOMER
                status = .CANCEL
              case .driverNotFound:
                stateStatusMessageTH = StateStatusMessage.driverNotFound.rawValue
                stateStatusMessageEN = StateStatusMessage.driverNotFound.rawValue
                state = .DRIVER
                status = .NOTFOUND
              case .driverTimeout:
                stateStatusMessageTH = StateStatusMessage.driverTimeout.rawValue
                stateStatusMessageEN = StateStatusMessage.driverTimeout.rawValue
                state = .DRIVER
                status = .TIMEOUT
              case .driverFound:
                stateStatusMessageTH = StateStatusMessage.driverFound.rawValue
                stateStatusMessageEN = StateStatusMessage.driverFound.rawValue
                state = .DRIVER
                status = .FOUND
              case .merchantTimeout:
                stateStatusMessageTH = StateStatusMessage.merchantTimeout.rawValue
                stateStatusMessageEN = StateStatusMessage.merchantTimeout.rawValue
                state = .MERCHANT
                status = .TIMEOUT
              case .merchantReject:
                stateStatusMessageTH = StateStatusMessage.merchantReject.rawValue
                stateStatusMessageEN = StateStatusMessage.merchantReject.rawValue
                state = .MERCHANT
                status = .REJECT
              case .merchantAccept:
                stateStatusMessageTH = StateStatusMessage.merchantAccept.rawValue
                stateStatusMessageEN = StateStatusMessage.merchantAccept.rawValue
                state = .MERCHANT
                status = .ACCEPT
              case .driverDeparture:
                stateStatusMessageTH = StateStatusMessage.driverDeparture.rawValue
                stateStatusMessageEN = StateStatusMessage.driverDeparture.rawValue
                state = .DRIVER
                status = .DEPARTURE
              case .driverWaiting:
                stateStatusMessageTH = StateStatusMessage.driverWaiting.rawValue
                stateStatusMessageEN = StateStatusMessage.driverWaiting.rawValue
                state = .DRIVER
                status = .WAITING
              case .driverPickedUp:
                stateStatusMessageTH = StateStatusMessage.driverPickedUp.rawValue
                stateStatusMessageEN = StateStatusMessage.driverPickedUp.rawValue
                state = .DRIVER
                status = .PICKEDUP
              case .driverNearBy:
                stateStatusMessageTH = StateStatusMessage.driverNearBy.rawValue
                stateStatusMessageEN = StateStatusMessage.driverNearBy.rawValue
                state = .DRIVER
                status = .NEARBY
              case .driverArrived:
                stateStatusMessageTH = StateStatusMessage.driverArrived.rawValue
                stateStatusMessageEN = StateStatusMessage.driverArrived.rawValue
                state = .DRIVER
                status = .ARRIVED
              case .driverComplete:
                stateStatusMessageTH = StateStatusMessage.driverComplete.rawValue
                stateStatusMessageEN = StateStatusMessage.driverComplete.rawValue
                state = .DRIVER
                status = .COMPLETE
              case .adminCancel:
                stateStatusMessageTH = StateStatusMessage.adminCancel.rawValue
                stateStatusMessageEN = StateStatusMessage.adminCancel.rawValue
                state = .ADMIN
                status = .CANCEL
              }
            })
          }
        } label: {
          VStack(spacing: 5){
            HStack{
              Text(stateStatusCase.rawValue.isEmpty ? placeholder : stateStatusCase.rawValue)
                .foregroundColor(stateStatusCase.rawValue.isEmpty ? .gray : .black)
                Spacer()
                Image(systemName: "chevron.down")
                    .foregroundColor(Color.green)
                    .font(Font.system(size: 20, weight: .bold))
            }
            .padding(.horizontal)
            Rectangle()
              .fill(Color.green)
              .frame(height: 2)
          }
        }
        
        HStack {
          Text("State: " + state.rawValue).foregroundColor(.green)
          Text("Status: " + status.rawValue).foregroundColor(.green)
        }
        
        VStack(alignment: .leading, spacing: 0) {
          VStack(alignment: .leading, spacing: 0) {
            Text("th: ")
            TextEditor(text: $stateStatusMessageTH)
              .multilineTextAlignment(.leading)
              .lineLimit(5)
              .font(.body)
              .frame(height: 80, alignment: .topLeading)
              .border(.black, width: 1)
              .cornerRadius(2)
              .foregroundColor(.black)
              .onChange(of: stateStatusMessageTH, perform: { value in
                
              })
          }
          
          VStack(alignment: .leading, spacing: 6) {
            Text("en: ")
            TextEditor(text: $stateStatusMessageEN)
              .multilineTextAlignment(.leading)
              .lineLimit(5)
              .font(.body)
              .frame(height: 80, alignment: .topLeading)
              .border(.black, width: 1)
              .cornerRadius(2)
              .foregroundColor(.black)
              .onChange(of: stateStatusMessageEN, perform: { value in
                
              })
          }
          
          VStack(alignment: .leading, spacing: 6) {
            Text("updateDatetime: ")
            TextField("updateDatetime: ", text: $updateDatetime, onEditingChanged: { changed in
              //on change
            }) {
              //on commit
            }.textFieldStyle(RoundedBorderTextFieldStyle())
          }
          
          VStack(alignment: .leading, spacing: 6) {
            Text("enableCancel: ")
            Menu {
              ForEach(enableCancelValue, id: \.self){ value in
                Button("\(value.description)") {
                  self.enableCancel = value
                }
              }
            } label: {
                VStack(spacing: 5){
                    HStack{
                      Text("\(enableCancel.description)")
                        .foregroundColor(enableCancel ? .red : .green)
                        Spacer()
                        Image(systemName: "chevron.down")
                        .foregroundColor(Color.yellow)
                        .font(Font.system(size: 20, weight: .bold))
                    }
                    .padding(.horizontal)
                    Rectangle()
                    .fill(Color.yellow)
                    .frame(height: 2)
                }
            }
            
            Text("Type: ")
            Menu {
              ForEach(typeList, id: \.self){ value in
                Button("\(value.rawValue)") {
                  self.type = value
                }
              }
            } label: {
                VStack(spacing: 5){
                    HStack{
                      Text("\(type.rawValue)")
                        Spacer()
                        Image(systemName: "chevron.down")
                        .font(Font.system(size: 20, weight: .bold))
                    }
                    .padding(.horizontal)
                    Rectangle()
                    .frame(height: 2)
                }
            }
            
            Text("Channel: ")
            Menu {
              ForEach(channelList, id: \.self){ value in
                Button("\(value.rawValue)") {
                  self.channel = value
                }
              }
            } label: {
                VStack(spacing: 5){
                    HStack{
                      Text("\(channel.rawValue)")
                        Spacer()
                        Image(systemName: "chevron.down")
                        .font(Font.system(size: 20, weight: .bold))
                    }
                    .padding(.horizontal)
                    Rectangle()
                    .frame(height: 2)
                }
            }
            
            Text("Collection name: \(collectionName)")
            Menu {
              ForEach(collectionNameValue, id: \.self){ collectionName in
                Button(collectionName) {
                  self.collectionName = collectionName
                }
              }
            } label: {
                VStack(spacing: 5){
                    HStack{
                      Text(collectionName)
                        .foregroundColor(.black)
                        Spacer()
                        Image(systemName: "chevron.down")
                        .foregroundColor(Color.blue)
                        .font(Font.system(size: 20, weight: .bold))
                    }
                    .padding(.horizontal)
                    Rectangle()
                    .fill(Color.blue)
                    .frame(height: 2)
                }
            }
          }
          
          Text("Environment: \(environment)")
          Menu {
            ForEach(environmentValue, id: \.self){ env in
              Button(env) {
                self.environment = env
              }
            }
          } label: {
              VStack(spacing: 5){
                  HStack{
                    Text(environment)
                      .foregroundColor(.black)
                      Spacer()
                      Image(systemName: "chevron.down")
                      .foregroundColor(Color.blue)
                      .font(Font.system(size: 20, weight: .bold))
                  }
                  .padding(.horizontal)
                  Rectangle()
                  .fill(Color.blue)
                  .frame(height: 2)
              }
          }
        }
        
        TextField("Order ID: ", text: $orderId, onEditingChanged: { changed in
          //on change
        }) {
          //on commit
        }.textFieldStyle(RoundedBorderTextFieldStyle())
        
        Button("Call [\(collectionName)\(environment)]", action: {
          if state.rawValue != "",
             status.rawValue != "",
             orderId != "",
             collectionName != "",
             environment != "",
             stateStatusMessageTH != "",
             stateStatusMessageEN != "",
             channel.rawValue != "",
             type.rawValue != "" {
            let en = RequestBodyModel.EN(stringValue: stateStatusMessageEN)
            let th = RequestBodyModel.TH(stringValue: stateStatusMessageTH)
            let wordingFields = RequestBodyModel.WordingFields(th: th, en: en)
            let mapValue = RequestBodyModel.MapValue(fields: wordingFields)
            let statusDescription: RequestBodyModel.StatusDescription = RequestBodyModel.StatusDescription(mapValue: mapValue)
            let _updateDateTime = RequestBodyModel.UpdateDatetime(stringValue: updateDatetime)
            let _enableCancel = RequestBodyModel.EnableCancel(booleanValue: enableCancel)
            let fields: RequestBodyModel.Fields = RequestBodyModel.Fields(status: status,
                                                                          state: state,
                                                                          statusDescription: statusDescription,
                                                                          updateDateTime: _updateDateTime,
                                                                          enableCancel: _enableCancel,
                                                                          channel: channel,
                                                                          type: type)
            result = viewModel.callFireStore(collectionName: collectionName + environment, orderId: orderId, fields: fields)
            showingAlert = true
          }
        }).buttonStyle(.bordered).alert(result, isPresented: $showingAlert) {
          Button("OK", role: .cancel) {
            
          }
        }.font(.system(size: 14))
        
      }.padding().frame(width: UIScreen.main.bounds.width,
                        height: UIScreen.main.bounds.height*1.2,
                        alignment: .topLeading)
      
    }).tabItem {
      Image(systemName: "table")
      Text("FireStore")
    }
    .onTapGesture {
      UIApplication.shared.endEditing()
    }
    //End FireStore
  }//End body
}//End FireStore: View
