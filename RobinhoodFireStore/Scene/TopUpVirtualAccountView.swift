//
//  TopUpVirtualAccountView.swift
//  RobinhoodFireStore
//
//  Created by Purananunaka, Chanon on 27/6/2565 BE.
//

import SwiftUI
import Combine

struct TopUpVirtualAccountView: View {
  @State private var showingAlert = false
  @State var result: String = ""
  @ObservedObject var viewModel = ViewModel()
  
  @State var userId: String = "a62c3e79-6b5a-4256-bb5c-8575906ff959-TsWx3jmF"
  @State var currency: RequestBodyForVAModel.Currencies = .THB
  var currencyValue: [RequestBodyForVAModel.Currencies] = [.THB]
  @State var amount: String = "0"
  @State var channel: RequestBodyForVAModel.Channel = .SPEND
  var chennelValue: [RequestBodyForVAModel.Channel] = [.SPEND]
  @State var service: RequestBodyForVAModel.Service = .FOOD
  var serviceValue: [RequestBodyForVAModel.Service] = [.FOOD,.MART]
  
  var body: some View {
    //Begin VA
    ScrollView(.vertical, showsIndicators: true, content: {
      VStack(alignment: .leading, spacing: 4) {
        
        VStack(alignment: .leading, spacing: 0) {
          Text("UserId: ")
          TextEditor(text: $userId)
            .multilineTextAlignment(.leading)
            .lineLimit(5)
            .font(.body)
            .frame(height: 80, alignment: .topLeading)
            .border(.black, width: 1)
            .cornerRadius(2)
            .foregroundColor(.black)
            .onChange(of: userId, perform: { value in
              
            })
        }
        
        Text("Currency: ")
        Menu {
          ForEach(currencyValue, id: \.self){ value in
            Button("\(value.stringValue)") {
              self.currency = value
            }
          }
        } label: {
            VStack(spacing: 5){
                HStack{
                  Text("\(currency.stringValue)")
                    Spacer()
                    Image(systemName: "chevron.down")
                    .foregroundColor(Color.primary)
                    .font(Font.system(size: 20, weight: .bold))
                }
                .padding(.horizontal)
                Rectangle()
                .fill(Color.primary)
                .frame(height: 2)
            }
        }
        
        VStack(alignment: .leading, spacing: 0) {
          Text("Amount: (\(currency.rawValue))")
          TextEditor(text: $amount)
            .multilineTextAlignment(.leading)
            .lineLimit(5)
            .font(.body)
            .frame(height: 80, alignment: .topLeading)
            .border(.black, width: 1)
            .cornerRadius(2)
            .foregroundColor(.black)
            .onChange(of: amount, perform: { value in
              
            })
        }
        
        Text("Channel: ")
        Menu {
          ForEach(chennelValue, id: \.self){ value in
            Button("\(value.stringValue)") {
              self.channel = value
            }
          }
        } label: {
            VStack(spacing: 5){
                HStack{
                  Text("\(channel.stringValue)")
                    Spacer()
                    Image(systemName: "chevron.down")
                    .foregroundColor(Color.primary)
                    .font(Font.system(size: 20, weight: .bold))
                }
                .padding(.horizontal)
                Rectangle()
                .fill(Color.primary)
                .frame(height: 2)
            }
        }
        
        Text("Service: ")
        Menu {
          ForEach(serviceValue, id: \.self){ value in
            Button("\(value.stringValue)") {
              self.service = value
            }
          }
        } label: {
            VStack(spacing: 5){
                HStack{
                  Text("\(service.stringValue)")
                    Spacer()
                    Image(systemName: "chevron.down")
                    .foregroundColor(Color.primary)
                    .font(Font.system(size: 20, weight: .bold))
                }
                .padding(.horizontal)
                Rectangle()
                .fill(Color.primary)
                .frame(height: 2)
            }
        }
        
        Button("Top up \(amount) \(currency.stringValue) on \(channel.stringValue) \(service.stringValue) ", action: {
          
          if userId != "",
             currency.rawValue != "",
             amount != "",
             channel.rawValue != "",
             service.rawValue != "" {
            let fields: RequestBodyForVAModel.Fields = RequestBodyForVAModel.Fields(userId: userId,
                                                                                    currency: currency,
                                                                                    amount: amount,
                                                                                    channel: channel,
                                                                                    service: service)
            result = viewModel.callTopUpVA(fields: fields)
            showingAlert = true
          }
        }).buttonStyle(.bordered).alert(result, isPresented: $showingAlert) {
          Button("OK", role: .cancel) {
            
          }
        }.font(.system(size: 14))
        
      }.padding().frame(width: UIScreen.main.bounds.width,
                        height: UIScreen.main.bounds.height,
                        alignment: .topLeading)
    }).tabItem {
      Image(systemName: "creditcard")
      Text("Top Up VA")
    }
    .onTapGesture {
      UIApplication.shared.endEditing()
    }
    //End VA
  }
}
