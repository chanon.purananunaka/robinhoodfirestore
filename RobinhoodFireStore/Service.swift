//
//  Service.swift
//  RobinhoodFireStore
//
//  Created by Purananunaka, Chanon on 11/3/2565 BE.
//

import Foundation
import Combine
import Alamofire
import SystemConfiguration

protocol ServiceProtocol {
  func callFireStore(collectionName: String, orderId: String, fields: RequestBodyModel.Fields) -> String//AnyPublisher<DataResponse<String, Error>, Never>
  func simpleCallFireStore(collectionName: String, orderId: String, fields: RequestBodyModel.Fields) -> String
  func topUpVirtualAccount(fields: RequestBodyForVAModel.Fields) -> String
  func sendNotification(fields: RequestBodyForNotificationModel.Fields) -> String
}


class Service {
    static let shared: ServiceProtocol = Service()
    private init() { }
}

extension Service: ServiceProtocol {
  func simpleCallFireStore(collectionName: String, orderId: String, fields: RequestBodyModel.Fields) -> String {
    var result: String = ""
    
    var semaphore = DispatchSemaphore (value: 0)
    let parameters = "{\n\"fields\":\n{\n\"status\": {\"stringValue\": \"\(fields.status.stringValue)\"},\n\"state\": {\"stringValue\": \"\(fields.state.stringValue)\"},\n\"statusDescription\":{ \n    \"mapValue\": { \n        \"fields\": { \n            \"th\": {\"stringValue\": \"\(fields.statusDescription.mapValue.fields.th.stringValue)\"},\n            \"en\": {\"stringValue\": \"\(fields.statusDescription.mapValue.fields.en.stringValue)\"}\n        }\n    }\n},\n\"updateDatetime\":{\"stringValue\": \"\(fields.updateDateTime.stringValue)\"},\n\"enableCancel\":{\"booleanValue\": \(fields.enableCancel.booleanValue) },\n\"payment\": {\n            \"mapValue\": {\n                \"fields\": {\n                    \"channel\": {\n                        \"stringValue\": \"\(fields.channel.rawValue)\"\n                    },\n                    \"type\": {\n                        \"stringValue\": \"\(fields.type.rawValue)\"\n                    }\n                }\n            }\n        }\n}\n}"
    
    let postData = parameters.data(using: .utf8)
    print("CPK: \(parameters)")

    var request = URLRequest(url: URL(string: "https://firestore.googleapis.com/v1beta1/projects/robinhood-f74c9/databases/(default)/documents/\(collectionName)/\(orderId)?updateMask.fieldPaths=status&updateMask.fieldPaths=state&updateMask.fieldPaths=statusDescription&updateMask.fieldPaths=updateDatetime&updateMask.fieldPaths=enableCancel&updateMask.fieldPaths=payment")!,timeoutInterval: Double.infinity)
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")

    request.httpMethod = "PATCH"
    request.httpBody = postData

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard let data = data else {
        print("CPK: "+String(describing: error))
        semaphore.signal()
        return
      }
      print("CPK: "+String(data: data, encoding: .utf8)!)
      result = String(data: data, encoding: .utf8) ?? ""
      semaphore.signal()
    }

    task.resume()
    semaphore.wait()
    
    return result
  }
  
  func callFireStore(collectionName: String, orderId: String, fields: RequestBodyModel.Fields) -> String {//AnyPublisher<DataResponse<String, Error>, Never> {
    
    let param: [String: Any] = [
      "fields": [
        "status": [ "stringValue": fields.status.stringValue ],
        "state": [ "stringValue": fields.state.stringValue ],
        "statusDescription": [
          "mapValue": [
            "fields": [
              "th": [ "stringValue": fields.statusDescription.mapValue.fields.th.stringValue ],
              "en": [ "stringValue": fields.statusDescription.mapValue.fields.en.stringValue ]
            ]
          ]
        ],
        "updateDatetime": [ "stringValue": fields.updateDateTime.stringValue ],
        "enableCancel": [ "booleanValue": fields.enableCancel.booleanValue ]
      ]
    ]
    
    let url = URL(string: "https://firestore.googleapis.com/v1beta1/projects/robinhood-f74c9/databases/(default)/documents/\(collectionName)/\(orderId)?updateMask.fieldPaths=status&updateMask.fieldPaths=state&updateMask.fieldPaths=statusDescription&updateMask.fieldPaths=updateDatetime&updateMask.fieldPaths=enableCancel")!
    
    var result: String = ""
    
    let request = AF.request(url,
               method: .patch,
               parameters: param,
               encoding: JSONEncoding.default)
      .validate()
    
    print("CURL: \(request.cURLDescription())")
    
//    request
//      .responseDecodable(completionHandler: { decodeData in
//        decodeData.result
//      })
    
//      .responseString(completionHandler: { response in
//        result = "\(response.result)"
//        print("CPK: \(result)")
//      })
//      .responseJSON(completionHandler: { response in
//        result = "\(response.result)"
//        print("CPK: \(result)")
//      })
    
    result = simpleCallFireStore(collectionName: collectionName, orderId: orderId, fields: fields)
    return result
  }
  
  func topUpVirtualAccount(fields: RequestBodyForVAModel.Fields) -> String {
    var result: String = ""
    
    var semaphore = DispatchSemaphore (value: 0)
    let parameters = "{\n    \"currency\": \"\(fields.currency.rawValue)\",\n    \"amount\": \(fields.amount),\n    \"channel\": \"\(fields.channel.rawValue)\",\n    \"service\": \"\(fields.service.rawValue)\"\n}"
    let postData = parameters.data(using: .utf8)
    print("CPK: \(parameters)")

    var request = URLRequest(url: URL(string: "http://pocket-dev.alp-robinhood.com/v1/pocket/cash-in")!,timeoutInterval: Double.infinity)
    request.addValue(fields.userId, forHTTPHeaderField: "userid")
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("test_jo", forHTTPHeaderField: "correlationid")

    request.httpMethod = "POST"
    request.httpBody = postData

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard let data = data else {
        print("CPK: "+String(describing: error))
        semaphore.signal()
        return
      }
      print("CPK: "+String(data: data, encoding: .utf8)!)
      result = String(data: data, encoding: .utf8) ?? ""
      semaphore.signal()
    }

    task.resume()
    semaphore.wait()
    
    return result
  }
  
  func sendNotification(fields: RequestBodyForNotificationModel.Fields) -> String {
    var result: String = ""
    var semaphore = DispatchSemaphore (value: 0)
    let parameters = "{\n\"category\": \(fields.category.rawValue),\n\"subjectTh\": \"\(fields.subjectTh)\",\n\"subjectEn\": \"\(fields.subjectEn)\",\n\"bodyTh\": \"\(fields.bodyTh)\",\n\"bodyEn\": \"\(fields.bodyEn)\",\n\"deeplinkType\": \(fields.deeplinkType.rawValue),\n\"deeplinkLabelTh\": \"\(fields.deeplinkLabelTh)\",\n\"deeplinkLabelEn\": \"\(fields.deeplinkLabelEn)\",\n\"deeplinkTh\": \"\(fields.deeplinkTh)\",\n\"deeplinkEn\": \"\(fields.deeplinkEn)\",\n\"clickAction\": \"\(fields.clickAction)\",\n\"notificationType\": \(fields.notificationType.rawValue),\n\"prority\": null\n}"
    print("CPK: JSON => \(parameters)")
    let postData = parameters.data(using: .utf8)
    var request = URLRequest(url: URL(string: "https://b2b-apis-dev2.alp-robinhood.com/notification/v1/notification")!,timeoutInterval: Double.infinity)
    request.addValue("\(fields.userId)", forHTTPHeaderField: "userId")
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("173wGjkXnP6qKsKhCBUUWNUKeMr8YkG3B5Rez1Ya", forHTTPHeaderField: "x-api-key")
    request.addValue("noti_global_310821_06", forHTTPHeaderField: "correlationId")
    request.addValue("HTTPS/1.1", forHTTPHeaderField: "Upgrade")
    request.addValue("h1", forHTTPHeaderField: "ALPN")
    request.addValue("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36", forHTTPHeaderField: "User-Agent")
    request.httpMethod = "POST"
    request.httpBody = postData
    print("CPK: headers \(request.headers)")
    print("CPK: body \(request.httpBody?.description)")
    
    let config: URLSessionConfiguration = URLSessionConfiguration.default
    let session = URLSession(configuration: config)
    
    let task = session.dataTask(with: request) { data, response, error in //URLSession.shared.dataTask(with: request) { data, response, error in
      guard let data = data else {
        print("CPK: Error "+String(describing: error))
        semaphore.signal()
        return
      }
      print("CPK: data "+String(data: data, encoding: .utf8)!)
      result = String(data: data, encoding: .utf8) ?? ""
      semaphore.signal()
    }
    task.resume()
    semaphore.wait()
    return result
  }
  
//  func sendNotification(fields: RequestBodyForNotificationModel.Fields) -> String {
//    var result: String = ""
//    let urlString = "https://b2b-apis-dev2.alp-robinhood.com/notification/v1/notification"
//    let json = "{\n\"category\": \(fields.category.rawValue),\n\"subjectTh\": \"\(fields.subjectTh)\",\n\"subjectEn\": \"\(fields.subjectEn)\",\n\"bodyTh\": \"\(fields.bodyTh)\",\n\"bodyEn\": \"\(fields.bodyEn)\",\n\"deeplinkType\": \(fields.deeplinkType.rawValue),\n\"deeplinkLabelTh\": \"\(fields.deeplinkLabelTh)\",\n\"deeplinkLabelEn\": \"\(fields.deeplinkLabelEn)\",\n\"deeplinkTh\": \"\(fields.deeplinkTh)\",\n\"deeplinkEn\": \"\(fields.deeplinkEn)\",\n\"clickAction\": \"\(fields.clickAction)\",\n\"notificationType\": \(fields.notificationType.rawValue),\n\"prority\": null\n}"
//
//    let url = URL(string: urlString)!
//    let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
//
//    var request = URLRequest(url: url)
//    request.httpMethod = HTTPMethod.post.rawValue
//    request.setValue("\(fields.userId)", forHTTPHeaderField: "userId")
//    request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
//    request.setValue("173wGjkXnP6qKsKhCBUUWNUKeMr8YkG3B5Rez1Ya", forHTTPHeaderField: "x-api-key")
//    request.setValue("noti_global_310821_06", forHTTPHeaderField: "correlationId")
//    request.httpBody = jsonData
//    AF.request(request).responseJSON { (response) in
//      switch response.result {
//      case .success(let response):
//        result = "\(response)"
//      case .failure(let error):
//        result = error.localizedDescription
//      }
//      print(response)
//    }
//    return result
//  }
}
