//
//  RobinhoodFireStoreApp.swift
//  RobinhoodFireStore
//
//  Created by Purananunaka, Chanon on 11/3/2565 BE.
//

import SwiftUI

@main
struct RobinhoodFireStoreApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
