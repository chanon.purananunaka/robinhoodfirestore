//
//  UIApplicationExtension.swift
//  RobinhoodFireStore
//
//  Created by Purananunaka, Chanon on 28/6/2565 BE.
//

import Foundation
import SwiftUI

extension UIApplication {
  func endEditing() {
    sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
  }
}
