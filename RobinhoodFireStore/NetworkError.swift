//
//  NetworkError.swift
//  RobinhoodFireStore
//
//  Created by Purananunaka, Chanon on 11/3/2565 BE.
//

import Foundation
import Alamofire

struct NetworkError: Error {
  let initialError: AFError
  let backendError: BackendError?
}

struct BackendError: Codable, Error {
    var status: String
    var message: String
}
